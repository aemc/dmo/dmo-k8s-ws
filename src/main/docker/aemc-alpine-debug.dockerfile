FROM alpine:latest

# download/pull necessary "base" images (for COPY --from)
# podman image pull rancher/calico-ctl:v3.19.2

# export VERSION=20220424

# podman image build --squash \
#   -t aemc/alpine-debug:$VERSION \
#   -t aemc/alpine-debug:latest \
#   -f ./aemc-alpine-debug.dockerfile \
#   .

# podman login
# podman image push aemc/alpine-debug:latest
# podman image push aemc/alpine-debug:$VERSION
# podman logout

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories

RUN apk update
RUN apk add openssh-client curl
RUN apk add docker
RUN apk add tcpdump tcpreplay
RUN apk add bash bash-completion
RUN apk add zstd-dev libsasl
RUN apk add libcap
RUN rm -rf /var/cache/apk/*

COPY --from=rancher/calico-ctl:v3.19.2 /calicoctl /usr/bin/calicoctl
