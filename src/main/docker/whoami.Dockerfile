FROM aemc/alpine-debug:latest

WORKDIR /app
COPY --from=containous/whoami:latest /whoami /app/

RUN setcap 'cap_net_bind_service=+ep' /app/whoami

ENTRYPOINT [ "./whoami" ]
