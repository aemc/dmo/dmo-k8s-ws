#!/usr/bin/env bash
# set -eu

echo ""
echo "dbg01 -> whoami02"
kubectl exec -it -n dev01 dbg01 -- sh -c 'curl --connect-timeout 5 whoami02.dev02'

echo ""
echo "dbg02 -> whoami01"
kubectl exec -it -n dev02 dbg02 -- sh -c 'curl --connect-timeout 5 whoami01.dev01'

echo ""
echo "dbg02 -> whoami02"
kubectl exec -it -n dev02 dbg02 -- sh -c 'curl --connect-timeout 5 whoami02.dev02'

echo ""
echo "dbg02 -> heise.de"
kubectl exec -it -n dev02 dbg02 -- sh -c 'curl --connect-timeout 5 heise.de'

echo ""
