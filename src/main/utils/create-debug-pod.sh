#!/usr/bin/env bash
set -eu

cat << EOF | kubectl apply -f -
kind: Pod
apiVersion: v1
metadata:
  name: debug
spec:
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
          matchExpressions:
          - key: "app.k8s.io/name"
            operator: "In"
            values:
            - "whoami"
        topologyKey: "kubernetes.io/hostname"
  containers:
  - name: main
    image: aemc/whoami:latest
    env:
    - name: "POD_IP"
      value: ${POD_IP}
EOF
